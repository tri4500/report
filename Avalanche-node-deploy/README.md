# Node Deploy Report

## Node deploy flow

1. Fetch data chain P
1. Khi fetch xong data của chain P. Node tiếp tục fetch data của chain X và C. Nếu tx tạo subnet có trong whiteList của node xuất hiện trong data được fetch từ chain P về. Node cũng sẽ fetch data của subnet trên.

- Note:
  - Nếu node được peer với một số node chỉ định, thì node sẽ chỉ lấy data của các validator được peer với node. Vậy nên độ đầy đủ của data của các chain trong node sẽ phụ thuộc vào data của các validator trong danh sách peer của node.
  - Nếu node đã có data của chain P trước đó và trong đó có tx tạo subnet có trong white list. Khi chạy node thì node sẽ sinh ra RPC url cho subnet trên, nhưng chưa dùng được. Node phải thực hiện consensus và phải lấy data từ validator. Vì vậy, danh sách peer của node phải bao gồm ít nhất 1 validator.
  - Node chỉ đóng vai trò lưu trữ data. Vì vậy rpc url của node chỉ dùng để query data. Để thực hiện transaction thì phải thực hiện qua RPC của validator.

## Vấn đề lưu trữ data trên node và validator

1. Cách 1:

- Chỉ Peer node và validator trong subnet với với nhau
- `Lợi`: Chỉ cần fetch data của chain P từ block đầu tiên đến block chứa tx tạo subnet. Sau đó dừng lại và chỉ fetch data của riêng subnet của mình. Flow này áp dụng được cho tất cả validator và node có trong subnet. Giúp cho đỡ phải lưu data không cần thiết của phần còn lại của chain P và toàn bộ chain X và C.
- `Hại`: Validator và node chỉ peer nội bộ với nhau sẽ bị giảm thời gian uptime của node và validator. (Ava Labs yêu cầu uptime phải >80%. Trước đây khi peer 2 validator thì uptime chỉ đạt 65%->75%)

2. Cách 2:

- Validator sẽ cho peer với các node default của network fuji. Các node trong subnet sẽ config peer với các validator.
- `Lợi`: Thời gian uptime của các validator sẽ tăng.
- `Hại`: Tất cả các node và validator trong subnet sẽ phải fetch và xử lý thêm data của chain C,P,X.

## Run node with docker

**1. Docker compose:**

```yml
version: "3"
services:
  avalanchego-1:
    image: avaplatform/avalanchego:v1.9.1
    command:
      [
        "--chain-config-dir=/root/.avalanchego/configs/chains",
        "--config-file=/root/.avalanchego/configs/node.json",
        "--whitelisted-subnets=26sis61HxZ8pmiXmMn4Dd853u1y4ANiebs5jtViYBJU1U6C2G7",
        "--subnet-config-dir=/root/.avalanchego/configs/subnets",
      ]
    ports:
      - "9650:9650"
      - "9651:9651"
    networks:
      avalanche:
        ipv4_address: 10.5.0.2
    volumes:
      - ./build1:/avalanchego/build
      - ./data1:/root/.avalanchego

  avalanchego-2:
    image: avaplatform/avalanchego:v1.9.1
    command:
      [
        "--chain-config-dir=/root/.avalanchego/configs/chains",
        "--config-file=/root/.avalanchego/configs/node.json",
        "--whitelisted-subnets=26sis61HxZ8pmiXmMn4Dd853u1y4ANiebs5jtViYBJU1U6C2G7",
        "--subnet-config-dir=/root/.avalanchego/configs/subnets",
      ]
    ports:
      - "9652:9650"
      - "9653:9651"
    networks:
      avalanche:
        ipv4_address: 10.5.0.3
    volumes:
      - ./build2:/avalanchego/build
      - ./data2:/root/.avalanchego

networks:
  avalanche:
    name: avalanche
    driver: bridge
    ipam:
      config:
        - subnet: 10.5.0.0/16
          gateway: 10.5.0.1
```

- **Note**

1.  Giải thích command:

- `--chain-config-dir=/root/.avalanchego/configs/chains` : Chỉ định đường dẫn của folder chứa các file chain config. File chain config của 1 chain được quy định là {BlockChainID}.json. Chứa các thông số config của chain. ([Xem thêm ở đây](https://docs.avax.network/nodes/maintain/chain-config-flags)). File config mẫu đang dùng cho Doschain:

````json
{
"eth-apis": [
"eth",
"eth-filter",
"net",
"web3",
"internal-eth",
"internal-blockchain",
"internal-public-transaction-pool",
"internal-tx-pool",
"private-debug",
"public-debug",
"debug-tracer"
],
"log-level": "error"
}

    ```
````

- `--config-file=/root/.avalanchego/configs/node.json` : Chỉ định đường dẫn đến file node config. File json này khai báo một số thông số chung của node trên các chain bao gồm 3 chain mặc định C,X,P và các subnet trong whiteList cũng được khai báo trong file này. ([Xem thêm ở đây](https://docs.avax.network/nodes/build/set-up-node-with-installer#advanced-node-configuration)) File node config mẫu đang dùng cho Doschain:

```json
{
  "http-host": "",
  "network-id": "fuji",
  "public-ip": "192.168.62.234",
  "whitelisted-subnets": "26sis61HxZ8pmiXmMn4Dd853u1y4ANiebs5jtViYBJU1U6C2G7",
  "bootstrap-ids": "NodeID-Ej578hCdtGtaSQUMQFkPkoXVHHByJJzZM",
  "bootstrap-ips": "34.67.69.231:9650"
}
```

- `--whitelisted-subnets=26sis61HxZ8pmiXmMn4Dd853u1y4ANiebs5jtViYBJU1U6C2G7` : String các Subnet ID mà node muốn được tham gia vào. Nếu có nhiều subnet muốn join thì phân biết các subnet ID bằng dấu ','.

- `--subnet-config-dir=/root/.avalanchego/configs/subnets` : Chỉ định đường dẫn đến subnet config. Là một folder chứa các file json config của các subnet. Các thông số liên quan đến thuật toán đồng thuận dùng cho subnet. File subnet config của 1 subnet được quy định dạng {SubnetID}.json và nằm trong subnet-config folder. ([Xem thêm ở đây](https://docs.avax.network/nodes/maintain/subnet-configs)).

2. **Volume**

- Container cần link một số volume ra ngoài để có thể dùng chung hoặc sử dụng lại khi chạy node khác.

  **- volume Build:**

  1. Cấu trúc:

  ```
  * build
    * plugins
      * evm
      * afS5GSoukX9EvD5ADVDzrv4bEBFbQCdzJuMDDZK21eGX9DCc2
    * avalanchego
  ```

  2. Chi tiết:

     - Trong thự mục build gồm 1 folder plugins và 1 file thực thi avalanchego.
     - chứa các file evm binary ứng với các chain và subnet của node đang chạy. Trên ví dụ trên là gồm evm binary của evm chain C và evm binary của subnet có subnetId là `afS5GSoukX9EvD5ADVDzrv4bEBFbQCdzJuMDDZK21eGX9DCc2`

  3. Mapping:

     - Thự mục này sẽ map vơi thư mục `/avalanchego/build` của container. Thư mục này container chỉ đọc.

  **- volume Data:**

  1. Cấu trúc:

  ```
    * data
      * configs
        * node.json
        * subnets
          * {subnetID}.json
        *chains
          * C
            * config.json
          * BlockchainID
            * config.json
      * staking
        * signer.key
        * staker.crt
        * staker.key
  ```

  2. Chi tiết:

  - Thư mục data ban đầu sẽ có 2 folder configs và staking. Khi chạy, node sync data thì sẽ xuất hiện thêm 2 folder nữa là log và db.
  - Thư mục configs có 3 thư mục con.
    - **chains:** Chứa các file json chain config.
    - **subnets:** Chứa các file json subnet config.
    - **node.json:** File json config của node.
  - Thư mục staking chứa các file chứa publicKey và privateKey của node. Dựa vào các file này để biết được NodeID cũng như số AVAX đã stack của node. Do đó muốn backup node thì phải lưu lại các file này. Nếu chưa có key của node trước đó. Có thể để trống thư mục này, khi chạy sẽ gennerate ra key node và nodeId mới.
